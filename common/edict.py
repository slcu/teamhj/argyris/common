from typing import *

T = TypeVar('T')
U = TypeVar('U')

def unionWith(m1: Dict[T, U],
              m2: Dict[T, U],
              f: Callable[[U, U], U]) -> Dict[T, U]:
    combined = dict()

    combKeys = set.union(set(m1.keys()), set(m2.keys()))

    for k in combKeys:
        if k in m1 and k in m2:
            combined[k] = f(m1[k], m2[k])
        elif k in m1 and k not in m2:
            combined[k] = m1[k]
        elif k not in m1 and k in m2:
            combined[k] = m2[k]

    return combined

def unionsWith(ms: List[Dict[T, U]],
               f: Callable[[List[U]], U],
               df: U) -> Dict[T, U]:
    combined = dict()

    combKeys = set.union(*[set(m.keys()) for m in ms])

    for k in combKeys:
        combined[k] = f([m.get(k, df) for m in ms])

    return combined

def union(m1: Dict[T, U], m2: Dict[T, U]) -> Dict[T, U]:
    combined = dict()

    if set(m1.keys()).intersection(m2.keys()):
        raise ValueError("non distinct keys")

    combKeys = set.union(set(m1.keys()), set(m2.keys()))

    for k in combKeys:
        if k in m1:
            combined[k] = m1[k]
        elif k in m2:
            combined[k] = m2[k]

    return combined

def map(m: Dict[T, U], f: Callable[[U], U]) -> Dict[T, U]:
    return dict([(k, f(v)) for k, v in m.items()])

def mapWithKey(m: Dict[T, U], f: Callable[[T, U], U]) -> Dict[T, U]:
    return dict([(k, f(k, v)) for k, v in m.items()])

def relabel(m: Dict[T, U], kms: Dict[T, List[T]]) -> Dict[T, U]:
    m1 = dict()
    for k, v in m.items():
        for km in kms.get(k, []):
            m1[km] = v

    return m1

def filter(m: Dict[T, U], f: Callable[[U], bool]) -> Dict[T, U]:
    return dict([(k, v) for k, v in m.items() if f(v)])

def invert(m: Dict[T, U]) -> Dict[U, T]:
    return dict([(v, k) for k, v in m.items()])

def getDf(m: Dict[T, U], k: T) -> Optional[U]:
    return m.get(k, None)

def gets(m: Dict[T, U], ks: List[T]) -> List[U]:
    return [v for v in [getDf(m, k) for k in ks] if v]


def invertListD(m: Dict[T, List[U]]) -> Dict[U, T]:
    dd = {}
    for k, vs in m.items():
        for v in vs:
            dd[v] = k

    return dd
