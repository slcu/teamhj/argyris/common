from typing import *
from os.path import join
import os


def decode(fname, d=","):
    cs = []
    header = []
    with open(fname, "r") as fin:
        header = fin.readline().strip().split(d)

        for ln in fin:
            elems = ln.strip().split(d)
            c = dict(zip(header, elems))
            cs.append(c)

    return cs


def decode_noheader(fname, d=","):
    cs = []
    with open(fname, "r") as fin:
        for ln in fin:
            elems = ln.strip().split(d)
            cs.append(elems)

    return cs


def csv_filt(fn):
    return fn.endswith(".csv")


def read_dir(d, filt_f, info_f):
    file_nms = [f for f in os.listdir(d) if filt_f(f)]

    file_nms_ = []
    for fn in file_nms:
        file_nms_.append((join(d, fn), info_f(fn)))

    return file_nms_
